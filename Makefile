# the idea here is that we go through all the files recursively
# then take the filename and duplicate it while also putting it into a pandoc X -o X template
# then the next 2 do just /s/x/y but on the last rather then the first
# then we append the lua filters and template option
# and then we exec that string with bash
compile:
	@find ./markdown -type f \
		| sed 's/\(.*\)/pandoc \1 -o \1/' \
		| sed 's/\(.*\)md/\1html/' \
		| sed 's/\(.*\)markdown/\1html/' \
		| sed 's/\(.*\)/\1 --lua-filter=.\/filters\/emoji.lua --template=.\/templates\/main.html/' \
		| bash

cp_dirstructure:
	@find ./markdown -type d | sed 's/markdown/html/' | xargs mkdir -p

push:
	scp -r . celeste.mountain:/root/website.static

release:
	cp ./lotta.gay.website.service /etc/systemd/system/
	systemctl daemon-reload
	systemctl restart lotta.gay.website.service
	systemctl status lotta.gay.website.service

# @find ./assets/raw_emojis -type f -exec sh -c 'ffmpeg -i {} -vf scale=36:48 "${}.scaled.png"' {} \;
# @find ./assets/raw_emojis -type f -exec sh -c ' input="{}"; output=$(echo $input | sed "s/raw/processed/"); ffmpeg -i "$input" -vf scale=36:48 "$output" ' \;

# @find ./assets/raw_emojis -type f -exec sh -c 'input="$1"; output=$(echo "$input" | sed "s/raw/processed/"); ffmpeg -i "$input" -vf scale=36:48 "$output"' sh {} \;
# downscale_emojis:
# 	@find ./assets/raw_emojis -type f -exec sh -c 'I="$1"; O=$(echo "$I" | sed "s/raw/processed/"); echo $I' sh {} \;

