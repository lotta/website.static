---
title: about lotta
# summary:
# toc:
# date:
author: lotta
---
- pronouns: it/she
- likes: computers, frozen pizza, girls and enbies, anime and manga, hardcore (the punk thingy)
- dislikes: most sensory input, imprecise communcation, being mean and ignorant

## what you can expect here

- not that much tbh: 

  > "in the end, we simply do what everyone else does on the net: we advertise." \
  _ellen ullman - close to the machine_

- i want to write about projects that i am working on.
  - currently i have written up quite a bit on my journey to a plugin free neovim config (there are dependencies though)
  - i want to do a write up of my bachelors thesis once thats done 
    (its about how the labor supply of wives is impacted by income splitting)
- i might put up to things that i self host if i somehow think that i have the capacity for moderation 
  (which i doubt to be honest)

## latest posts

- [downmixing stereo with ffmpeg](/general/downmixing_stereo_with_ffmpeg.html)

## places you can find me:

- **[codeberg](https://codeberg.org/git_girl/)**
- **[fedi](https://mk.absturztau.be/@lotta)**
- **matrix:** @msg:lotta.gay
- **e-mail:** mail[at]lotta.gay
- **pgp-key**
    - **[keyserver](https://keys.openpgp.org)**:  keys.openpgp.org
    - **[keyserver.onion](http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/)**:  http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion
