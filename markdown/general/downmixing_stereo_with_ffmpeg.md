---
title: downmixing stereo with ffmpeg
summary: this post is just talking about 
toc: \
    <!--toc:start-->
    - [downmixing stereo with ffmpeg](#downmixing-stereo-with-ffmpeg)
      - [bash script that wraps ffmpeg](#bash-script-that-wraps-ffmpeg)
    <!--toc:end-->
date: 04.01.2024
author: lotta
---

this is not witchcraft but we found this really helpful - because panning in stereo audio is 
bad for us as a sensory input. however stereo mixes clean up the mix and we generally want to
honor the work that went into making them (it's also mainly about the movement of tracks in a
mix that eeks us out).

maybe i am also wrong on the term so for clarification with 'downmixing' i am referring to
reducing the percentage of stereo in the mix.

## bash script that wraps ffmpeg

we use this bashscript to downmix tracks on a track by track basis.

```bash
function downmix() {
    local file=$1
    local percentage=$2
    local out=$3

    if [ -f $file ]; then
        :
    else
        >&2 echo "file \"$file\" does not exist, aborting"
        return 1
    fi

    if [ $percentage -le 100 ] && [ $percentage -ge 0 ]; then
        :
    else
        >&2 echo "stereo downmix percentage \"$percentage\" not withing 0 and 100, aborting"
        return 1
    fi

    if [ -z "$out" ]; then
        out="downmix.$percentage.$file"
    fi

    ffmpeg -i $file -af "pan=stereo|c0<c0 + $percentage * c1|c1< $percentage * c0+c1" $out
}
```

## experince with live downmixing

generally a project we have planned is having this work well 'live'. so an audio effect -
that is preferrably adjustable via a midi controller (that we want to build ourselves).

we did try using [`easyeffects`](https://github.com/wwmm/easyeffects). with that we ran into
issues about getting weird audio artefacts that kinda sounded like interference patterns.
this might just be due to the latency settings i set, so maybe that's something for you.
as the name suggests, it was also pretty easy to set up which is a plus.

for the future we want to realize this with a pipewire filter chain. examples can be found
in `/usr/share/pipewire/filter-chain/` (given you have pipewire installed).

## resources i used

- [ffmpeg docs on audio](https://trac.ffmpeg.org/wiki/AudioChannelManipulation)
- [stackoverflow quesiton with great explanation](https://stackoverflow.com/questions/14498539/how-to-overlay-downmix-two-audio-files-using-ffmpeg)
- [on downmixing](https://bobpariseau.com/blog/2018/6/20/understanding-audio-downmix-and-surround-sound-processing-or-wait-ive-got-the-wrong-number-of-speakers) (lol haven't read it)
