-- parse <emoji NAME/>  => <img source="/assets/emoji/NAME.png" style="width: 1em; height: 1em;" />

function Pandoc(doc)
  for _, block in ipairs(doc.blocks) do

    if block.t == "Para" then
      -- Iterate through inline elements in the paragraph
      for i, inline in ipairs(block.content) do
        -- Check if the inline element is a RawInline element and contains the custom tag
        if inline.t == "RawInline" and inline.text:match("<emoji%s+(%w+)%s*/>") then
          local emojiName = inline.text:match("<emoji%s+(%w+)%s*/>")
          -- Create the replacement image element
          local imgElement = pandoc.RawInline("html", string.format('<img src="/assets/emojis/%s.png" style="width: 1em; height: 1em"/>', emojiName))
          -- Replace the RawInline element with the img element
          block.content[i] = imgElement
        end
      end
    end
  end
  return doc
end

return {Pandoc = Pandoc}
